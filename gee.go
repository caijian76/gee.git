package gee

import (
	"net/http"
	"strings"
)

type HandlerFunc func(*Context)

type Engine struct {
	router *router
	*GroupRoute
	groups []*GroupRoute
}

func New() *Engine {
	engine := &Engine{router: newRouter()}
	engine.GroupRoute = &GroupRoute{engine: engine}
	engine.groups = []*GroupRoute{engine.GroupRoute}
	return engine

}

func Defaule() *Engine {
	engine := New()
	engine.USE(Logger(), Recovery())
	return engine
}

func (engine *Engine) GET(pattern string, handler HandlerFunc) {
	engine.router.addRoute("GET", pattern, handler)
}
func (engine *Engine) POST(pattern string, handler HandlerFunc) {
	engine.router.addRoute("POST", pattern, handler)
}

func (engine *Engine) Run(address string) {
	http.ListenAndServe(address, engine)
}

func (engine *Engine) Getroute(path string) {
	engine.router.getRoute("GET", path)
}
func (engine *Engine) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	var middlewares []HandlerFunc
	for _, group := range engine.groups {
		if strings.HasPrefix(req.URL.Path, group.prefix) {
			middlewares = append(middlewares, group.middlewares...)
		}
	}

	c := newContext(w, req)
	c.handlers = middlewares
	engine.router.handle(c)
}
